- [Página web con ejercicios de herencia mendeliana](https://biologia-geologia.com/BG4/35_problemas_de_genetica.html)
- [Imagen
  cromosoma](https://learn-genetics.b-cdn.net/disorders/singlegene/images/chr-gene-prot.jpg)
- [Single gene
  disorders](https://learn.genetics.utah.edu/content/disorders/singlegene/)
